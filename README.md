# Temperature sensor

## About Temperature sensor

**Temperature sensor** highlights the notion of sensor networks. Here it is a device which makes it possible to capture the temperature of an environment and launch an alarm when the latter exceeds a certain critical threshold: in our case 25°C. The language used here is C++. Indeed this is a mini operating system (TinyOs) widely used in sensor networks.

## About TinyOS
What is TinyOS? TinyOS is an open source, BSD-licensed operating system designed for low-power wireless devices, such as those used in sensor networks, ubiquitous computing, personal area networks, smart buildings, and smart meters. A worldwide community from academia and industry use, develop, and support the operating system as well as its associated tools, averaging 35,000 downloads a year. To find out more, see : 
- **[http://www.tinyos.net/](http://www.tinyos.net/)**
- **[Wikipedia](https://en.wikipedia.org/wiki/TinyOS)**
- **[This Youtube tutorial](https://www.youtube.com/watch?v=TLw9Ydy9OKM)**

**Note** : to achieve this work I did it thanks to an online tool called [tinkercad](https://www.tinkercad.com/). You can also use it for the realization of your circuits and as part of the sensor network and the Internet of Things and also in other fields such as electronics for example. 

## About [tinkercad](https://www.tinkercad.com/)

Tinkercad is a free-of-charge, online 3D modeling program that runs in a web browser. Since it became available in 2011 it has become a popular platform for creating models for 3D printing as well as an entry-level introduction to constructive solid geometry in schools

**[see more](https://en.wikipedia.org/wiki/Tinkercad)**

## execution 

this mini project is much more practical, you can get the **arduino kit** and directly implement the source code which is available in this gitlab. In my case I did the simulation with [tinkercad](https://www.tinkercad.com/) as I told you more. It's much more practical for those who can't bring the **Arduino Kit**. This kit is available on the website of **[Amazon](https://www.amazon.fr/s?k=kit+arduino+uno&adgrpid=61889753651&hvadid=275331476117&hvdev=c&hvlocint=1006094&hvlocphy=9070075&hvnetw=g&hvqmt=e&hvrand=17620570759097248054&hvtargid=kwd-295316372147&hydadcr=27710_1756304&tag=googhydr0a8-21&ref=pd_sl_5rv9ogkr28_e)**