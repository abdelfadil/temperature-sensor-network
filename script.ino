// C++ code
//Programme du Capteur

int capt = 0; //capteur de temperature branché sur l'entré analogique 0
int freq = 200; //frequence d'alarme fixé à 330Hz

void setup()
{
  Serial.begin(9600); //etablir la liaison serial avec l'ordinateur
  pinMode[4, OUTPUT];//led verte
  pinMode[8, OUTPUT];//led rouge
}

void loop()
{
  int valcapt = analogRead(capt);
  float volt = valcapt*5.0/1024.0;
  float temp = (volt - 0.5)*100;
  Serial.print("temperature : ");
  Serial.print(temp);
  Serial.println("°C");
  delay(2000);

  if(temp > 25){
    digitalWrite(4, LOW); // led verte eteinte
    digitalWrite(8, HIGH); // led rouge allumée
    tone(3, freq);
  }else{
    digitalWrite(4, HIGH); // led verte allumée
    digitalWrite(8, LOW); // led rouge eteinte
    noTone(3);
  }
}